# configure TOL

# look for tol
find_package( TOL REQUIRED )
if( TOL_FOUND )
  message( STATUS "TOL_INCLUDE_DIR = ${TOL_INCLUDE_DIR}" )
  message( STATUS "TOL_LIBRARIES = ${TOL_LIBRARIES}" )
else( TOL_FOUND )
endif( TOL_FOUND )

add_subdirectory( source )
